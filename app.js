'use strict';

const express = require('express');
const parseArgs = require('minimist');
const fs = require('fs');
var readFile = require('util').promisify(fs.readFile);

var app = express();

const args = parseArgs(process.argv.slice(2));

if (!args.f || typeof args.f !== 'string') {
    console.error('Exepected Args: -f <config file>');
    process.exit(1);
}

var sensors = [];
var temps = {};

app.get("/", function (req, res) {
    res.json(temps);
});

var port = process.env.PORT || 5000;
app.listen(port, function () {
    console.log("Server running on port " + port);
});

readFile(args.f)
    .then(configTxt => {
        var s;
        try {
            s = JSON.parse(configTxt.toString());
        }
        catch (err) {
            throw new Error('config file:', args.f, "Parsing error.");
        }
        if (!Array.isArray(s)) {
            throw new Error('config file does not contain an array');

        }
        s.forEach(e => {
            if (e.name && e.dev)
                sensors.push({ name: e.name, dev: e.dev });
            else {
                throw new Error('Malformed option in config file: ', e);
            }
        })

        // Start polling them
        setInterval(() => {
            sensors.forEach(s => {
                readSensor(s.dev)
                    .then(r => {
                        console.log(`Sensor ${s.name} (${s.dev}) reported ${r}`);
                        temps[s.name] = r;
                    })
                    .catch(err => console.log(`Error reading sensor ${s}: ${err}`));
            });
        }, 10000);
    })
    .catch(err => {
        console.error('Error:', err);
        process.exit(1);
    });

function readSensor(s) {
    return readFile(s)
        .then(data => {
            var match = /[\s\S]*t=(\d{3,6})/.exec(data.toString());
            return (match ? match[1] / 1000 : null);
        });
}